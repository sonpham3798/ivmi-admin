import { contractService, userService } from "../_services"
import { settings } from "../config"

export const contractModule = {
    namespaced: true,
    state: {
        contracts: {
            state: { loading: false }
        },
        userContract: {
            state: { loading: false },
            data: []
        },
        contract: {
            state: { loading: false },
            data: {}
        }
    },
    mutations: {
        contractsRequest(state) {
            state.contracts = {
                state: { loading: true }
            }
        },
        contractsSuccess(state, data) {
            state.contracts = {
                state: { success: true },
                data: data
            }
        },
        contractsFailure(state, message) {
            state.contracts = {
                state: { success: false },
                message: message
            }
        },

        /**
         * 
         */
        userContractRequest(state) {
            state.userContract = {
                state: { loading: true },
                data: []
            }
        },
        userContractSuccess(state, data) {
            state.userContract = {
                state: { success: true },
                data: data
            }
        },
        userContractFailure(state, message) {
            state.userContract = {
                state: { success: true },
                message: message,
                data: []
            }
        },

        /**
         * 
         */
        contractRequest(state) {
            state.contract = {
                state: { loading: true },
                data: {}
            }
        },
        contractSuccess(state, data) {
            state.contract = {
                state: { success: true },
                data: data
            }
        },
        contractFailure(state, message) {
            state.contract = {
                state: { success: false },
                data: null,
                message: message
            }
        }
    },
    actions: {
        getListContracts({ commit }, size, page) {
            commit('contractsRequest');
            contractService.getContracts(size, page).then(success => {
                let data = success.body;
                if (data.status === settings.SUCCESS) {
                    commit('contractsSuccess', data.data);
                } else {
                    commit('contractsFailure', data.message);
                }
            }).catch(error => {
                commit('contractsFailure', `${error.status} - ${error.statusText}`);
            });
        },
        getUserContracts({ commit }, username, size, page) {
            commit('userContractRequest');
            userService.getUserContracts(username, size, page).then(success => {
                let data = success.body;
                if (data.status === settings.SUCCESS) {
                    commit('userContractSuccess', data.data);
                } else {
                    commit('userContractFailure', data.message);
                }
            }).catch(error => {
                commit('userContractFailure', `${error.status} - ${error.statusText}`)
            });
        },
        getContractId({ commit }, id) {
            commit('contractRequest');
            contractService.getContractByID(id).then(success => {
                let data = success.body;
                if (data.status === settings.SUCCESS) {
                    let contract = data.data;
                    // get user of contract info                    
                    userService.getUserByUsername(contract.username).then(res => {
                        if (res.body.status === settings.SUCCESS) {
                            contract.user = res.body.data;
                            // send to state
                            commit('contractSuccess', contract);
                        } else {
                            console.error(res.body.message);
                            commit('contractFailure', res.body.message);
                        }
                    }).catch((error) => {
                        console.error(error);
                        commit('contractFailure', '')
                    });

                } else {
                    commit('contractFailure', data.message);
                }
            }).catch(error => {
                commit('contractFailure', `${error.status} - ${error.statusText}`)
            });
        }
    }
}