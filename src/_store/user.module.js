import { userService } from "../_services/user.service"
import { settings } from "../config"

export const userModule = {
    namespaced: true,
    getters: {
        isLoading: state => state.update.state.loading || state.allUser.state.loading 
                                                        || state.newUser.state.loading 
                                                        || state.userByUsername.state.loading
    },
    state: {
        allUser: {
            state: { loading: false },
            total: 0,
            users: []
        },
        newUser: {
            state: { loading: false },
            data: ""
        },
        blockUser: {
            state: { loading: false }
        },
        userByUsername: {
            state: { loading: false }
        },
        update: {
            state: { loading: false }
        }
    },
    mutations: {
        /**
         * Get list user
         */
        allUserRequest(state) {
            state.allUser = {
                state: { loading: true }
            }
        },
        allUserFailure(state, message) {
            state.allUser = {
                state: { success: false },
                message: message
            }
        },
        allUserSuccess(state, users, total) {
            state.allUser = {
                state: { success: true },
                total: total,
                users: users
            }
        },

        /**
         * Create new user
         */
        newUserRequest(state) {
            state.newUser = {
                state: { loading: true }
            }
        },
        newUserFailure(state, message) {
            state.newUser = {
                state: { success: false },
                message: message
            }
        },
        newUserSuccess(state, data) {
            state.newUser = {
                state: { success: true },
                data: data
            }
        },

        /**
         * Block User
         */
        blockUserRequest(state) {
            state.blockUser = {
                state: { loading: true }
            }
        },
        blockUserFailure(state, message) {
            state.blockUser = {
                state: { success: false },
                message: message
            }
        },
        blockUserSuccess(state, message) {
            state.blockUser = {
                state: { success: true },
                message: message
            }
        },
        blockUserResetState(state) {
            state.blockUser = {
                state: { loading: false }
            }
        },

        /**
         * Get User By ID
         */
        userByUsernameRequest(state) {
            state.userByUsername = {
                state: { loading: false }
            }
        },
        userByUsernameSuccess(state, data) {
            state.userByUsername = {
                state: { success: true },
                data: data
            }
        },
        userByUsernameFailure(state, message) {
            state.userByUsername = {
                state: { success: false },
                message: message
            }
        },

        /**
         * Update user
         */
        setUpdateState(state, obj) {
            state.update = obj
        }
    },
    actions: {
        getAllUser({ commit }, { size, page }) {
            commit('allUserRequest');
            userService.getAllUser(size, page).then(success => {
                let data = success.body;
                if (data.status === settings.SUCCESS) {
                    commit('allUserSuccess', data.data, data.total);
                } else {
                    commit('allUserFailure', data.message);
                }
            }).catch(error => {
                commit('allUserFailure', `${error.status} - ${error.statusText}`);
            });
        },

        createNewUser({ commit }, { user }) {
            commit('newUserRequest');
            userService.create(user).then(success => {
                let data = success.body;
                if (data.status === settings.SUCCESS) {
                    commit('newUserSuccess', data.data);
                } else {
                    commit('newUserFailure', data.message);
                }
            }).catch(error => {
                commit('newUserFailure', `${error.status} - ${error.statusText}`);
            });
        },

        blockUser({ commit }, { username }) {
            commit('blockUserRequest');
            userService.blockUser(username).then(response => {
                let data = response.body;

                if (data.status === settings.SUCCESS) {
                    commit('blockUserSuccess', data.message);
                } else {
                    commit('blockUserFailure', data.message);
                }

            }).catch(error => {
                commit('blockUserFailure', `${error.status} - ${error.statusText}`);
            });
        },

        blockUserResetState({ commit }) {
            commit('blockUserResetState');
        },

        getUserByUsername({ commit }, username) {
            commit('userByUsernameRequest');
            userService.getUserByUsername(username).then(success => {
                let data = success.body;
                if (data.status === settings.SUCCESS) {
                    commit('userByUsernameSuccess', data.data);
                } else {
                    commit('userByUsernameFailure', data.message);
                }
            }).catch(error => {
                commit('userByUsernameFailure', `${error.status} - ${error.statusText}`);
            });
        },

        updateUser({ commit }, { username, body }) {
            commit('setUpdateState', { state: { loading: true } });
            userService.updateUser(username, body).then(success => {
                let data = success.body;
                if (data.status === settings.SUCCESS) {
                    commit('setUpdateState', { state: { success: true }, message: "Update success", data: data.data });
                } else {
                    commit('setUpdateState', { state: { success: false }, message: "Update failed" });
                }
            }).catch(error => {
                commit('setUpdateState', { state: { success: false }, message: `${error.status} - ${error.statusText}` })
            });
        }
    }
}