import { settings } from '../config';
import moment from 'moment';

export const SettingsMixin = {
    data: () => ({
        settings: settings,
    }),
    methods: {
        convertDateTime(dateTime, format = "DD-MM-YYYY") {
            let date = new Date(Date.parse(dateTime));
            return moment(date).format(format);
        },

        getStatusContract(durationDate) {
            let now = new Date(new Date().toLocaleDateString())
            let date = new Date(new Date(durationDate).toLocaleDateString());
            console.log(now>=date);
            return now > date;
        },

        currencyFormater(currency) {
            var formatter = new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND',
            });

            return formatter.format(currency);
        }
    }
}