import { settings } from '../config';

export const ContractMixin = {
    data: () => ({
        a: 1000000
    }),
    methods: {
        calcProgress(start, duration) {
            start = Date.parse(start).toString();
            duration = Date.parse(duration).toString();
            let current = new Date().getTime();
            let full = duration - start;
            let now = current - start;
            let percent = (now / full) * 100;
            return Number(percent).toFixed(3);
        },
        contractTitle(status) {
            let result = settings.CONTRACT.ONGOING;
            switch (status) {
                case 2:
                    result = settings.CONTRACT.EXPRIED;
                    break;
                case 3:
                    result = settings.CONTRACT.COMING_SOON;
                    break;
                case 4:
                    result = settings.CONTRACT.PAUSED;
                    break;
                default:
                    break;
            }
            return result;
        },
        contractColor(status) {
            let result = "primary";
            switch (status) {
                case 2:
                    result = "error";
                    break;
                case 3:
                    result = "success";
                    break;
                case 4:
                    result = "warning";
                    break;
                default:
                    break;
            }
            return result;
        },
    }
}