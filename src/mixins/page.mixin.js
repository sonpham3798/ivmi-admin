
export const PageMixin = {
    data: () => ({
        size: 10,
        page: 1
    }),
    methods: {

    },
    computed: {
        // size() {
            

        //     return 20;
        // },
        // page() {
            

        //     return 1;
        // }
    },
    mounted() {
        let size = this.$route.query.size;
        if (size !== undefined) {
            this.size = Number(size);
        } else {
            this.size = 36;
        }
        

        let page = this.$route.query.page;
        if (page !== undefined) {
            this.page =  Number(page);
        } else {
            this.page = 1;
        }
        
    }

}