import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store.js'
import vuetify from './plugins/vuetify';
import Snotify, { SnotifyPosition } from 'vue-snotify';

import { SettingsMixin } from './mixins'
import 'vue-snotify/styles/material.css';

Vue.config.productionTip = false;

const options = {
  toast: {
    position: SnotifyPosition.leftBottom,
    timeout: 3000,
    showProgressBar: false
  }
}
Vue.use(Snotify, options);

Vue.mixin(SettingsMixin);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
