import Vue from 'vue'
import VueRouter from 'vue-router'
import { cookies } from '../config'

Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      redirect: {name: 'User Management'}
    },
    {
      path: '/home',
      name: 'Home',
      // meta: { title: 'Dashboard' },
      // component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue')
      redirect: {name: 'User Management'}
    },
    {
      path: '/login',
      name: 'Login',
      meta: { title: 'Login' },
      component: () => import(/* webpackChunkName: "login" */ '@/views/Login.vue')
    },
    {
      path: '/users',
      name: 'User Management',
      meta: { title: 'User Management' },
      component: () => import(/* webpackChunkName: "user" */ '../views/UserManagement.vue')
    },
    {
      path: '/vegetables',
      name: 'Vegetable',
      meta: { title: 'Vegetable' },
      component: () => import(/* webpackChunkName: "vegetable" */ '@/views/Vegetable.vue')
    },
    {
      path: '/contracts',
      name: 'Contracts Management',
      meta: { title: 'Vegetable' },
      component: () => import(/* webpackChunkName: "contract" */ '@/views/ContractManage.vue')
    },
    {
      path: '/images',
      name: 'Images',
      meta: { title: 'Image' },
      component: () => import(/* webpackChunkName: "image" */ '@/views/ImageManagement.vue')
    },
    {
      path: '/history-detect',
      name: 'History Detect',
      meta: { title: 'History Detect' },
      component: () => import(/* webpackChunkName: "history" */ '@/views/HistoryDetect.vue')
    },
    {
      path: '/register',
      name: 'Register',
      meta: { title: 'Register' },
      component: () => import(/* webpackChunkName: "register" */ '@/views/Register.vue')
    },
    {
      path: '/request',
      name: 'Request',
      meta: { title: 'Request' },
      component: () => import(/* webpackChunkName: "request" */ '@/views/RequestManage.vue')
    },
    {
      path: '/register-contract',
      name: 'Register Contract',
      meta: { title: 'Register Contract' },
      component: () => import(/* webpackChunkName: "register_contract" */ '@/views/RegisterContract.vue')
    },
    {
      path: '/purchase',
      name: 'Purchase',
      meta: { title: 'Purchase' },
      component: () => import(/* webpackChunkName: "purchase" */ '@/views/Purchase.vue')
    },
    {
      path: '/purchase/:token',
      name: 'Purchase Token',
      meta: { title: 'Purchase' },
      component: () => import(/* webpackChunkName: "purchase" */ '@/views/Purchase.vue')
    },
    {
      path: '/request-page',
      name: 'Request page',
      meta: { title: 'Request page' },
      component: () => import(/* webpackChunkName: "request_page" */ '@/views/RequestPage.vue')
    }
  ]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  document.title = `${to.meta.title} | IVMI Panel`;

  const unauthPages = ['/login'];
   const publicPages = ['/register', '/purchase', '/request-page'];

  const authRequired = !unauthPages.includes(to.path);
  const hasLoggedIn = cookies.isKey('User');
  const allAccess = publicPages.includes(to.path);

  // if path is a public pages
  if (allAccess) {
    return next();
  }

  // if path have to log in.
  if (!hasLoggedIn && authRequired) {
    return next('/login');
  }

  if (!authRequired && hasLoggedIn) {
    return next('/home');
  }



  next();
});

export default router
