import { authHeader, request } from "../config";

const auth = { headers: authHeader() };

function getAll() {
    return request.get(`equipments`, auth);
}

export const equipmentService = {
    getAll
}