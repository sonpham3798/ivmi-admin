import { authHeader, request } from "../config";

const auth = { headers: authHeader() };

function getImageFromCamera() {
    return request.local_get('CallScriptPython', auth);
}

function getAllImages(size = 10, page = 1) {
    return request.get(`images?size=${size}&page=${page}`, auth);
}

export const imageService = {
    getImageFromCamera,
    getAllImages
}