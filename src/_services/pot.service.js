import { authHeader, request } from "../config";

const auth = { headers: authHeader() };

function updateCoordinates(id) {
    return request.local_patch(`pots/${id}`, null, auth);
}

function getPotByPosition(username, position) {
    return request.get(`pots/position?username=${username}&position=${position}`, auth);
}

function viewCoordinates(id) {
    return request.local_get(`pots/view-coordinate/${id}`, auth);
}

export const potService = {
    updateCoordinates,
    getPotByPosition,
    viewCoordinates
}
