import { authHeader, request } from "../config";

const auth = { headers: authHeader() };

function getAll() {
    return request.get(`subscriptions`, auth);
}

export const subscriptionService = {
    getAll
}