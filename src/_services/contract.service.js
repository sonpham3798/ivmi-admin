import { request, authHeader } from "../config"

const auth = { headers: authHeader() };

function getContracts(size = 10, page = 1) {
    return request.get(`contracts?size=${size}&page=${page}`, auth);
}

function getContractByID(id) {
    return request.get(`contracts/${id}`, auth);
}

function updateContract(id, contract) {
    return request.put(`contracts/${id}`, contract, auth);
}

function createNewContract(body) {
    return request.post(`contracts`, body, auth);
}

function deleteUserContract(id) {
    return request.delete(`contracts/${id}?mode=true`, auth);
}

function pause(id) {
    return request.patch(`contracts/${id}/pause`, null, auth);
}

function getContractFromRenewToken(token) {
    return request.get(`contracts/renew/${token}`, auth);
}

export const contractService = {
    getContracts,
    getContractByID,
    updateContract,
    createNewContract,
    deleteUserContract,
    pause,
    getContractFromRenewToken
}