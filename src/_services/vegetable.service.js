import { request, authHeader } from "../config";

const auth = { headers: authHeader() };

function getListVegetables(size, page) {
    return request.get(`vegetables?size=${size}&page=${page}`, auth);
}

function create(body) {
    return request.post(`vegetables`, body, auth);
}

function remove(id) {
    return request.delete(`vegetables/${id}?mode=true`, auth);
}

export const vegetableService = {
    getListVegetables,
    create,
    remove
}