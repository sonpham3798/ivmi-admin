import { authHeader, request } from "../config";

const auth = { headers: authHeader() };

function getScheduleById(id) {
    return request.get(`schedules/${id}`, auth);
}

export const scheduleService = {
    getScheduleById
}
