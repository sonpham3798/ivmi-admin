import { request, authHeader, settings } from "../config"

const auth = { headers: authHeader() };

function create(user) {
    return request.post('users/create', user, auth);
}

function getAllUsers() {
    return request.get('users', auth);
}

function getAllUser(size, page) {
    return request.get(`users/get-all-users?size=${size}&page=${page}`, auth);
}

function blockUser(username) {
    let url = `users/${username}/block-unblock/${settings.BLOCK_USER}`;
    return request.patch(url, null, auth);
}

function unblockUser(username) {
    let url = `users/${username}/block-unblock/${settings.UN_BLOCK_USER}`;
    return request.patch(url, null, auth);
}

function getUserByUsername(username) {
    return request.get(`users/${username}`, auth);
}

function getUserContracts(username, size = 10, page = 1) {
    return request.get(`users/${username}/contracts?size=${size}&page=${page}`, auth);
}

function updateUser(username, body) {
    return request.put(`users/${username}`, body, auth);
}

function getUserPlots(username) {
    return request.get(`users/${username}/plots?show_pots=true`, auth);
}

function getUserSchedules(username) {
    return request.get(`users/${username}/schedules`, auth);
}

function startUserSchedules(username) {
    return request.local_get(`users/${username}/schedules/start`, auth);
}

function stopUserSchedules(username) {
    return request.local_get(`users/${username}/schedules/stop`, auth);
}

function getUserContractLastestDuration(username) {
    return request.get(`users/${username}/contracts/lastest?type=duration`, auth);
}

function search(key, type = 'name') {
    return request.local_get(`users/search?type=${type}&q=${key}`, auth);
}

function getContractByUsername(username) {
    return request.get(`users/${username}/contract`, auth);
}

export const userService = {
    create,
    getAllUser,
    getAllUsers,
    blockUser,
    unblockUser,
    getUserByUsername,
    getUserContracts,
    updateUser,
    getUserPlots,
    getUserSchedules,
    startUserSchedules,
    stopUserSchedules,
    getUserContractLastestDuration,
    search,
    getContractByUsername
}