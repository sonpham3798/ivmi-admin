import { request } from '../config';

export function login(user) {
    return request.post('users/login', user, { headers: { 'Content-Security-Policy': 'block-all-mixed-content' } });
}