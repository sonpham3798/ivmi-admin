import { authHeader, request } from "../config";

const auth = { headers: authHeader() };

function create(body) {
    console.log(body);
    return request.post(`requests`, body, auth);
}

function getAll(size, page) {
    return request.get(`requests?size=${size}&page=${page}`, auth);
}

function updateStatus(id, status) {
    return request.patch(`requests/${id}?status=${status}`, null, auth);
}

function getById(id) {
    return request.get(`requests/${id}`, auth);
}

export const requestService = {
    create,
    getAll,
    updateStatus,
    getById
}