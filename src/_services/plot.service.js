import { authHeader, request } from "../config";

const auth = { headers: authHeader() };

function create(body) {
    return request.post(`plots`, body, auth);
}

function remove(id) {
    return request.delete(`plots/${id}?mode=true`, auth);
}

export const plotService = {
    create,
    remove
}
