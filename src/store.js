import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import { contractModule, userModule } from './_store';

export default new Vuex.Store({
  modules: {
    contractModule,
    userModule
  }
})
