import Vue from 'vue';
import VueResource from 'vue-resource';

Vue.use(VueResource);

const SERVER_URL = 'https://important.cosplane.asia/api';
// const SERVER_URL = 'https://localhost:44364/api';
const LOCAL_SERVER_URL = 'https://localhost:44364/api';


export const request = {
    get(url, config = null) {
        return Vue.http.get(`${SERVER_URL}/${url}`, config);
    },
    post(url, body = null, config = null) {
        return Vue.http.post(`${SERVER_URL}/${url}`, body, config)
    },
    put(url, body = null, config = null) {
        return Vue.http.put(`${SERVER_URL}/${url}`, body, config)
    },
    delete(url, config = null) {
        return Vue.http.delete(`${SERVER_URL}/${url}`, config)
    },
    patch(url, body = null, config = null) {
        return Vue.http.patch(`${SERVER_URL}/${url}`, body, config);
    },
    local_patch(url, body = null, config = null) {
        return Vue.http.patch(`${LOCAL_SERVER_URL}/${url}`, body, config);
    },
    local_get(url, config = null) {
        return Vue.http.get(`${LOCAL_SERVER_URL}/${url}`, config);
    },
}