import Vue from 'vue';
import VueCookies from 'vue-cookies';

Vue.use(VueCookies);

const expireTimes = '1d';

export const cookies = {
    set,
    get,
    isKey,
    remove,
    keys
}

function set(keyName, value) {
    return VueCookies.set(keyName, value, expireTimes);
}

function get(keyName) {
    return VueCookies.get(keyName);
}

function isKey(keyName) {
    return VueCookies.isKey(keyName);
}

function remove(keyName) {
    return VueCookies.remove(keyName);
}

function keys() {
    return VueCookies.keys();
}