
export const settings = {
    SUCCESS: 'SUCCESS',

    DIALOG_MAX_WIDTH: '500px',
    DIALOG_MAX_WIDTH_SMALL: '350px',
    DIALOG_MAX_WIDTH_2: '700px',
    DIALOG_MAX_WIDTH_3: '600px',
    DIALOG_CONFIRM_MAX_WIDTH: '350px',
    DIALOG_CONTRACT_MAX_WIDTH: '400px',

    BLOCK_USER: 1,
    UN_BLOCK_USER: 2,

    CONTRACT: {
        COMING_SOON: 'Sắp diễn ra',
        ONGOING: 'Đang diễn ra',
        EXPRIED: 'Hết hạn',
        PAUSED: 'Dừng hợp đồng'
    },

    ROLES: {
        admin: {
            role: 2,
            title: "Quản trị viên"
        },
        user: {
            role: 1,
            title: "Người dùng thông thường"
        }
    },

    POTS: {
        EMPTY: 'Chưa trồng cây',
        PLANT: 'Cây Đang trồng',
        DEAD_PLANT: 'Cây bị chết',
        HARVEST: ''
    },

    PLOTS: {
        MAX: 5,
        POT_SIZE: [4, 5, 6, 7, 8]
    },
    REQUESTS: {
        PENDING: 'Đang chờ xử lý', // 1
        CONFIRM: 'Xác nhận đang xử lý', // 2
        DONE: 'Đã hoàn tất', // 3
        REJECT: 'Hủy yêu cầu', // 4
        // POST_PAY: 'Thanh toán sau', // 5
        POST_PAY: 'Đang chờ xử lý', // 5
        PAID_AND_PENDING: 'Đã thanh toán và đang chờ' // 6
    },
    REQUESTS_STATUS_NUM: {
        PENDING: 1, // 1
        CONFIRM: 2, // 2
        DONE: 3, // 3
        REJECT: 4, // 4
        POST_PAY: 5, // 5
        PAID_AND_PENDING: 6 // 6
    },
    REQUESTS_TYPE: {
        SET_UP: 1,
        PURCHASE: 2
    },
    REQUESTS_TYPE_TEXT: {
        SET_UP: 'Thiết lập',
        PURCHASE: 'Gia hạn'
    },
    BUDGETS: {
        TYPE_1: 200000,
    },
    EQUIPMENTS: {
        PLOT: 5,
        SHELVING_PLASTIC: 8
    }
}