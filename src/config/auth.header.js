import { cookies } from "./cookies";


const loggedIn = cookies.isKey('User');

export function authHeader() {
    let result = {};
    if (loggedIn) {
        const user = cookies.get('User');
        
        result = {
            Authorization: `Bearer ${user.token}`,
            'Content-Security-Policy': 'block-all-mixed-content'
        }
    }
    return result;
}